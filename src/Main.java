package src;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        switch(i) {
            case (1): // Вызов первой задачи Array-3 > canBalance
                Array3CanBalance array3CanBalance = new Array3CanBalance();
                array3CanBalance.execute();
                break;

            case (2): // Вызов второй задачи Array-3 > maxSpan
                Array3MaxSpan array3MaxSpan = new Array3MaxSpan();
                array3MaxSpan.execute();
                break;

            case (3): // Вызов третьей задачи String-3 > maxBlock
                String3MaxBlock string3MaxBlock = new String3MaxBlock();
                string3MaxBlock.execute();
                break;

            case (4): // Вызов четвертой задачи String-3 > sumNumbers
                String3SumNumbers string3SumNumbers = new String3SumNumbers();
                string3SumNumbers.execute();
                break;
        }

    }
}
