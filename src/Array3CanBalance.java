package src;

// Array-3 > canBalance
// https://codingbat.com/prob/p158767

public class Array3CanBalance {

    public boolean canBalance(int[] nums) {
        int rightSum = 0;
        int leftSum = 0;

        for (int i = 0; i < nums.length; i++) {
            rightSum += nums[i];
        }

        for (int i = 0; i < nums.length ; i++) {
            rightSum -= nums[i];
            leftSum += nums[i];
            if (rightSum == leftSum)
                return true;
        }
        return false;
    }

    public void execute() {
        System.out.println(canBalance(new int[] {1, 1, 1, 2, 1}));
        System.out.println(canBalance(new int[] {2, 1, 1, 2, 1}));
        System.out.println(canBalance(new int[] {10, 10}));
    }
}
