package src;

// String-3 > sumNumbers
// https://codingbat.com/prob/p121193

public class String3SumNumbers {
    public int sumNumbers(String str) {
        int len = str.length();
        String line = "";
        int finish = 0;
        for (int i = 0; i < len ; i++) {
            if (Character.isDigit(str.charAt(i))) {
                if (i < len - 1 && Character.isDigit(str.charAt(i+1))) {
                    line += str.charAt(i);
                }
                else {
                    line += str.charAt(i);
                    finish += Integer.parseInt(line);
                    line = "";
                }
            }
        }
        return finish;
    }

    public void execute() {
        System.out.println(sumNumbers("abc123xyz"));
        System.out.println(sumNumbers("aa11b33"));
        System.out.println(sumNumbers("7 11"));
    }

}
