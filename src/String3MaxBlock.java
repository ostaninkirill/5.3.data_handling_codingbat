package src;

// String-3 > maxBlock
// https://codingbat.com/prob/p179479

public class String3MaxBlock {
    public int maxBlock(String str) {
        int count = 1;
        int finish = 1;

        if (str.isEmpty()) return 0;

        for (int i = 0; i < str.length(); i++) {
            if (i != str.length()-1 && str.charAt(i) == str.charAt(i+1)) {
                count++;
            }
            else {
                if (count > finish) {
                    finish = count;
                }
                count = 1;
            }
        }
        return finish;
    }

    public void execute() {
        System.out.println(maxBlock("hoopla"));
        System.out.println(maxBlock("abbCCCddBBBxx"));
        System.out.println(maxBlock(""));
    }

}
