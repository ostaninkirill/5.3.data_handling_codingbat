package src;

// Array-3 > maxSpan
// https://codingbat.com/prob/p189576

public class Array3MaxSpan {
    public int maxSpan(int[] nums) {
        int count = 0;
        int span = 0;

        for (int i = 0; i < nums.length ; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (nums[i] == nums[j]) count = j - i + 1;
                if (count > span) span = count;
            }
        }
        return span;
    }

    public void execute() {
        System.out.println(maxSpan(new int[] {1, 2, 1, 1, 3}));
        System.out.println(maxSpan(new int[] {1, 4, 2, 1, 4, 1, 4}));
        System.out.println(maxSpan(new int[] {1, 4, 2, 1, 4, 4, 4}));
    }
}
